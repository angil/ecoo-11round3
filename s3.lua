--[[ s3.lua Topographical problem from ECOO Finals '11
Jamie Langille
May 15/2011
Unfinished]]

-- Fills any '-' with the next letter around a space

function fillAround(grid, x, y, ch)
	for i = x - 1, x + 1 do
		for j = y - 1, y + 1 do
			if (not(i < 1 or j < 1 or i > #grid or j > #grid[1])
				and not(i == x and j == y)
				and grid[i][j] == '-') then
				grid[i][j] = string.char(1 + ch)
			end
		end
	end
end

-- Fills an interior with dashes
function floodFill(grid, x, y)
	if (x < 1 or y < 1
		or x > #grid
		or y > #grid[1]
		or grid[x][y] ~= '.') then
		return
	end
	grid[x][y] = '-'
	floodFill(grid, x-1, y)
	floodFill(grid, x, y-1)
	floodFill(grid, x+1, y)
	floodFill(grid, x, y+1)
end

--Main method
io.input("DATA31.txt")

for rep = 1, 5 do

	-- Read-in data for each of 5 test cases
	l,w,x,y = io.read("*n", "*n", "*n", "*n", "*l")
	print(l,w,x,y)
	grid={}
	for i = 1, l do
		grid[i]={}
		nzxt = io.read("*l")
		for j = 1, w do
			grid[i][j] = string.sub(nzxt, j, j)
		end
	end

	-- Fill
	floodFill(grid, x, y)

	-- Topographical Alg.
	for ch = string.byte('a'), string.byte('a') do
		for i = 1, l do
			for j = 1, w do
				if (grid[i][i] == string.char(ch)) then
					fillAround(grid, i, j, ch)
				end
			end
		end
	end

	-- Print array
	for i = 1, l do
		for j = 1, w do
			io.write(grid[i][j])
		end
		io.write("\n")
	end
	io.write("\n")
end

io.input():close()
