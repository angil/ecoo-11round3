import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class s2 {
	
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA21.txt"));
		
		LinkedList<Character> c1o = new LinkedList<Character>();
		char[] s1 = "DBUZ.SNPVLKCGJOYEIWTQMFXARH".toCharArray();
		for (char c : s1)
			c1o.add(c);
		LinkedList<Character> c2o = new LinkedList<Character>();
		char[] s2 = "FIYGZKJHABTSCDXVRN.LUMWQOEP".toCharArray();
		for (char c : s2)
			c2o.add(c);
		for (int n = 0; n < 5; n++) {
			LinkedList<Character> c1 = new LinkedList<Character>(c1o);
			LinkedList<Character> c2 = new LinkedList<Character>(c2o);
			
			int pos2, pos1;
			char let1, let, p;
			String m = "";
			
			String l = in.nextLine();
			for (int i = 0; i < l.length(); i++) {
				// if(l.charAt(i)=='.')
				pos2 = c2.indexOf(l.charAt(i));
				let1 = (char) (pos2 + 'A');
//				System.err.println(let1);
				if (let1 == '[')
					let1 = '.';
				pos1 = c1.indexOf(let1);
				let = (char) (pos1 + 'A');
				if (let == '[')
					let = '.';
				m = m + let;
				p = c1.pop();
				c1.add(p);
				if (p == 'H') {
					c2.add(c2.pop());
				}
			}
			
			System.out.println(m);
		}
		in.close();
	}
}
