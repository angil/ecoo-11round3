import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class s1 {
	
	private static void output(ArrayList<Integer> xCoords,
			ArrayList<Integer> yCoords, ArrayList<Character> chars) {
		int currentBook = 0;
		int lowest = Integer.MAX_VALUE;
		if (xCoords.size() == 1) {
			System.out.print(chars.get(0) + " ");
		} else {
			currentBook = mindex(xCoords, yCoords);
			// for (int i = 0; i < xCoords.size(); i++) {
			// if (xCoords.get(i) < lowest) {
			// lowest = xCoords.get(i);
			// currentBook = i;
			// } else if (xCoords.get(i) == lowest) {
			// if (yCoords.get(i) < lowest) {
			// lowest = xCoords.get(i);
			// currentBook = i;
			// }
			// }
			//
			// if (yCoords.get(i) < lowest) {
			// lowest = yCoords.get(i);
			// currentBook = i;
			// } else if (yCoords.get(i) == lowest) {
			// if (xCoords.get(i) < lowest) {
			// lowest = yCoords.get(i);
			// currentBook = i;
			// }
			// }
			// }
			
			xCoords.remove(currentBook);
			yCoords.remove(currentBook);
			char yo = chars.get(currentBook);
			chars.remove(currentBook);
			
			output(xCoords, yCoords, chars);
			System.out.print(yo + " ");
			
		}
	}
	
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA11.txt"));
		for (int g = 0; g < 5; g++) {
			
			int numBooks = in.nextInt();
			ArrayList<Character> chars = new ArrayList<Character>(0);
			ArrayList<Integer> xCoords = new ArrayList<Integer>(0);
			ArrayList<Integer> yCoords = new ArrayList<Integer>(0);
			
			for (int i = 0; i < numBooks; i++) {
				int k = in.nextInt();
				int j = in.nextInt();
				xCoords.add(Math.max(k, j));
				yCoords.add(Math.min(k, j));
				chars.add((char) (i + 'A'));
			}
			
//			xCoords.add(Integer.MAX_VALUE);
//			yCoords.add(Integer.MAX_VALUE);
//			chars.add((char)(numBooks+'A'));
			
			output(xCoords, yCoords, chars);
			System.out.println("");
			
		}
		
		in.close();
		
	}
	
	private static int mindex(ArrayList<Integer> x, ArrayList<Integer> y) {
		int min = Integer.MAX_VALUE;
		
		for (int val : x)
			if (val < min)
				min = val;
		for (int val : y)
			if (val < min)
				min = val;
		int minsumdex = x.indexOf(min) < 0 ? y.indexOf(min) : x.indexOf(min);
		for (int i = 0; i < x.size(); i++)
			if (x.get(i) == min || y.get(i) == min)
				if ((x.get(i) + y.get(i)) < (x.get(minsumdex) + y.get(minsumdex)))
					minsumdex = i;
		return minsumdex;
	}
}
