import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class s4 {
	
	static int currMin;
	static short[][] grid;
	static int[][] min;
	final static short[] time = {1, 2, 4, 6, 8};
	
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA41.txt"));
		
		grid = new short[20][20];
		
		for (int i = 0; i < 5; i++) {
			min = new int[20][20];
			for (int j = 0; j < 20; j++)
				for (int k = 0; k < 20; k++)
					min[j][k] = Integer.MAX_VALUE;
			
			for (int j = 0; j < 20; j++) {
				char[] temp = in.nextLine().toCharArray();
				for (int k = 0; k < 20; k++)
					grid[j][k] = (short) (temp[k] - '0');
			}
			
			currMin = Integer.MAX_VALUE;
			System.out.printf("It took %d hours.%n", rec(new HashSet<Point>(), 0, new Point(0, 0), 0));
		}
		
		in.close();
	}
	
	/*
	 * dir 0=n/a 1=x+1 2=x-1 3=y+1 4=y-1
	 */
	private static int rec(Set<Point> noGo, int hrs, Point loc, int dir) {
		// System.err.println(loc);
		if (loc.x > 19 || loc.x < 0 || loc.y > 19 || loc.y < 0)
			return 9001;
		int temp = 0;
		switch (dir) {
			case 1 : {
				temp = time[Math.abs(grid[loc.x][loc.y]
						- grid[loc.x - 1][loc.y])];
				break;
			}
			case 2 : {
				temp = time[Math.abs(grid[loc.x][loc.y]
						- grid[loc.x + 1][loc.y])];
				break;
			}
			case 3 : {
				temp = time[Math.abs(grid[loc.x][loc.y]
						- grid[loc.x][loc.y - 1])];
				break;
			}
			case 4 : {
				temp = time[Math.abs(grid[loc.x][loc.y]
						- grid[loc.x][loc.y + 1])];
				break;
			}
		}
		hrs += temp;
		if (hrs >= currMin)
			return 9001;
		if (hrs >= min[loc.x][loc.y])
			return 9001;
		min[loc.x][loc.y] = hrs;
		if (loc.x == 19 && loc.y == 19) {
			currMin = hrs;
			return hrs;
		}
		if (noGo.contains(loc))
			return 9002;
		
		Set<Point> newGo = new HashSet<Point>(noGo);
		newGo.add(loc);
		return min(rec(newGo, hrs, new Point(loc.x + 1, loc.y), 1), rec(newGo, hrs, new Point(loc.x - 1, loc.y), 2), rec(newGo, hrs, new Point(loc.x, loc.y + 1), 3), rec(newGo, hrs, new Point(loc.x, loc.y - 1), 4));
	}
	
	private static int min(int... rec) {
		int min = Integer.MAX_VALUE;
		for (int i : rec)
			if (i < min)
				min = i;
		
		return min;
	}
}
