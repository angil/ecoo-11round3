import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class s1new {
	
	static ArrayList<Character> chars;
	
	private static class Area implements Comparable<Area> {
		
		int from;
		int to;
		int area;
		
		public Area(int f, int t, int a) {
			from = f;
			to = t;
			area = a;
		}
		
		public boolean equals(Object o) {
			if (o instanceof Area)
				return from == ((Area) o).from && to == ((Area) o).to
						&& area == ((Area) o).area;
			return false;
		}
		
		public boolean equalsF(Area a) {
			return from == a.from;
		}
		
		public boolean equalsT(Area a) {
			return to == a.to;
		}
		
		public boolean equalsFT(Area a) {
			return from == a.from || to == a.to || from == a.to || to == a.from;
		}
		
		public int compareTo(Area a) {
			return area - a.area;
		}
		
		public String toString() {
			return "" + area;
		}
		
	}
	
	private static ArrayList<Area> sums;
	private static ArrayList<Integer> x;
	private static ArrayList<Integer> y;
	
	public static void main(String[] args) throws Exception {
		Scanner in = new Scanner(new File("DATA11.txt"));
		for (int g = 0; g < 5; g++) {
			int numBooks = in.nextInt();
			sums = new ArrayList<Area>();
			chars = new ArrayList<Character>(0);
			x = new ArrayList<Integer>(0);
			y = new ArrayList<Integer>(0);
			
			for (int i = 0; i < numBooks; i++) {
				x.add(in.nextInt());
				y.add(in.nextInt());
				chars.add((char) (i + 'A'));
			}
			
			for (int i = 0; i < numBooks; i++) {
				for (int j = 0; j < numBooks; j++) {
					if (i != j)
						sums.add(new Area(i, j, Math.min(x.get(i), x.get(j))
								* Math.min(y.get(i), y.get(j))));
				}
			}
			
			Collections.sort(sums);
			Collections.reverse(sums);
			
			System.out.println(rec(sums));
			
		}
		
		in.close();
	}
	
	private static String rec(ArrayList<Area> sums) {
		ArrayList<Area> max = new ArrayList<Area>();
		ArrayList<Area> sums2 = new ArrayList<Area>(sums);
		for (int i = 0; i < sums.lastIndexOf(sums.get(0)); i++)
			max.add(sums.get(i));
		
		for(int i = 0; i < )
		
		return null;
	}
}
