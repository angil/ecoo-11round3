import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class s3 {
	
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA31.txt")); // 1
		for (int rep = 0; rep < 5; rep++) {
			int l = in.nextInt();
			int w = in.nextInt();
			char grid[][] = new char[l][w];
			int x = in.nextInt() - 1;
			int y = in.nextInt() - 1;
			for (int i = 0; i < grid.length; i++) {
				grid[i] = in.next().toCharArray();
			}
			floodFill(grid, x, y);
			
			for (char ch = 'a'; ch < 'z'; ch++) {
				for (int i = 0; i < grid.length; i++) {
					for (int j = 0; j < grid[0].length; j++) {
						if (grid[i][j] == ch)
							fillAround(grid, i, j, ch);
					}
				}
			}
			
			for (int i = 0; i < grid.length; i++) {
				for (int j = 0; j < grid[0].length; j++) {
					System.out.print(grid[i][j]);
				}
				System.out.println();
			}
			System.out.println();
			System.out.println();
		}
		in.close();
	}
	
	private static void fillAround(char[][] grid, int x, int y, char ch) {
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (!(i < 0 || j < 0 || i >= grid.length || j >= grid[0].length)
						&& !(i == x && j == y) && grid[i][j] == '-')
					grid[i][j] = (char) (ch + 1);
			}
		}
	}
	
	private static void floodFill(char[][] grid, int x, int y) {
		if (x < 0 || y < 0 || x >= grid.length || y >= grid[0].length
				|| grid[x][y] != '.')
			return;
		grid[x][y] = '-';
		floodFill(grid, x - 1, y);
		floodFill(grid, x, y - 1);
		floodFill(grid, x, y + 1);
		floodFill(grid, x + 1, y);
	}
}
